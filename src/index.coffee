import Vue from "vue"
import Vuex from "vuex"
import SuiVue from "semantic-ui-vue"
import "semantic-ui-css/semantic.min.css"

import app from "./index.vue"
import store from "./store.coffee"

import ArticleBlock from "./blocks/article-block.vue"
import CommentBlock from "./blocks/comment-block.vue"
import CommentForm from "./blocks/comment-form.vue"

Vue.component "ArticleBlock", ArticleBlock
Vue.component "CommentBlock", CommentBlock
Vue.component "CommentForm", CommentForm

Vue.use Vuex
Vue.use SuiVue

new Vue
    el: "#app"
    render: (h) -> h app
    store: store
